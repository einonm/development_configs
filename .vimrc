syntax on
set foldmethod=marker
"set foldmarker={,}

set background=dark
colorscheme solarized
set hlsearch

" For editing KERNEL code
set tabstop=8
set shiftwidth=8

" Text width for emails
set tw=72

" Stops cscope from complaining when opening up vim
set nocscopeverbose

" vim -b : edit binary using xxd-format!
augroup Binary
    au!
    au BufReadPre  *.bin let &bin=1
    au BufReadPost *.bin if &bin | %!xxd
    au BufReadPost *.bin set ft=xxd | endif
    au BufWritePre *.bin if &bin | %!xxd -r
    au BufWritePre *.bin endif
    au BufWritePost *.bin if &bin | %!xxd
    au BufWritePost *.bin set nomod | endif
augroup END

filetype on
set fileformat=unix

let c_space_errors=1
highlight WhitespaceEOL ctermbg=red guibg=red
match WhitespaceEOL /\s\+$/
"highlight DreadedTabs ctermbg=blue guibg=blue
"match DreadedTabs /\t/

set laststatus=2
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P

