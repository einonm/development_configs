# A tmux setup
#
# Copyright (C) 2013 Mark Einon mark.einon@gmail.com
#
#  * This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

tmux has-session -t dev

if [ $? != 0 ]; then
# START:new
tmux new-session -s dev -n code -d
# END:new
# START:keystopane
tmux send-keys -t dev:1 'cd ~' C-m
# END:keystopane
# START:newwindow
tmux new-window -n irssi -t dev
tmux send-keys -t dev:2 'irssi' C-m
# END:newwindow
# START:newwindow
tmux new-window -n auvmail -t dev
tmux send-keys -t dev:3 'mutt' C-m
# END:newwindow
# START:newwindow
tmux new-window -n browse -t dev
tmux send-keys -t dev:4 'elinks' C-m
# END:newwindow
# START:newwindow
tmux new-window -n tasks -t dev
tmux send-keys -t dev:5 'task shell' C-m
# END:newwindow
# START:newwindow
tmux new-window -n gmail -t dev
tmux send-keys -t dev:6 'mutt -F ~/.gmail-muttrc' C-m
# END:newwindow
# START:selectwindow
tmux select-window -t dev:3
# END:selectwidow
fi

tmux attach -t dev
